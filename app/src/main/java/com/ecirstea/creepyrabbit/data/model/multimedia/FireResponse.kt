package com.ecirstea.creepyrabbit.data.model.multimedia

data class FireResponse (
    var audioList: List<String> = emptyList()
        )
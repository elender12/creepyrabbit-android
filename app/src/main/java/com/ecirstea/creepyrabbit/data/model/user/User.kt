package com.ecirstea.creepyrabbit.data.model.user

data class User(

    val name: String = "",
    val username: String = "",
    var password: String = "",
    var email: String = "",

)
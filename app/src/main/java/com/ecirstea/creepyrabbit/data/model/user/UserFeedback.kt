package com.ecirstea.creepyrabbit.data.model.user

import java.util.*

data class UserFeedback(
    val username: String,
    val message: String,
    val date: String
)
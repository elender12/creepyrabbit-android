package com.ecirstea.creepyrabbit.utils

object Constants {
    const val AUDIO_COLLECTION = "audios"
    const val FAVS_COLLECTION = "favs"
    const val SHARED_PREF_FILE = "userData"
    const val NAME_KEY = "name_key"
    const val TOKEN_KEY = "token_key"
    const val LOG_TAG= "info"
    const val HUMANS_CATEGORY= "humans"
    const val SUPERNATURAL_CATEGORY= "supernatural"
    const val RULES_CATEGORY = "rules"
}